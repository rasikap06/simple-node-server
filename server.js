const express = require('express');

const app = express();

const port = 8000;

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(port,(err)=>{
    if(err)
        console.log(`Error in ${port}`);
    else
        console.log(`Server running on port ${port}`);
});